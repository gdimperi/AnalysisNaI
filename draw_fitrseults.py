#from ROOT import *
import argparse
import math
import os
import numpy as np

parser = argparse.ArgumentParser(description='')
parser.add_argument('--inputlist1', dest='inputlist1', type=str,
                    default='', help='input list file name')
parser.add_argument('--inputlist2', dest='inputlist2', type=str,
                    default='', help='input list file name')
parser.add_argument('-o','--outdir', dest='outdir', type=str,
                    default='', help='output dir name')
args = parser.parse_args()


tau1list = []
tau2list = []
frac1list = []
frac2list = []
T_list = []

for filename in open(args.inputlist1):
  T_list.append(float(os.path.basename(filename).split("_")[5].rstrip("k.txt\n")))
  for line in open(filename.strip("\n")):
    if line.startswith("tau1"):
      tau1 = 1./float(line.split()[2])
      tau1_err = 1./float(line.split("+/-")[1].strip())
      tau1list.append(tau1)

for filename in open(args.inputlist2):
  for line in open(filename.rstrip("\n")):
    if line.startswith("frac1"):
      frac1 = float(line.split()[2])
      #print line.split()[2]
      frac1list.append(frac1)
    elif line.startswith("frac2"):
      frac2 = float(line.split()[2])
      frac2list.append(frac2)
    elif line.startswith("tau2"):
      tau2 = 1./float(line.split()[2])
      tau2list.append(tau2)
    
print(frac1)
print(frac2)

print("T    tau1    tau2    frac2/frac1\n")
for i in range(0,len(tau1list)):
  print(str(T_list[i])+"   "+str(round(tau1list[i],2))+"    "+str(round(tau2list[i],2))+"     "+str(float(frac2list[i])/float(frac1list[i])))


