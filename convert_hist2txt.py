from ROOT import *
import argparse
import math
import os

parser = argparse.ArgumentParser(description='')
parser.add_argument('-i','--inputlist', dest='inputlist', type=str,
                    default='', help='input list file name')
parser.add_argument('-o','--outdir', dest='outdir', type=str,
                    default='', help='output dir name')
args = parser.parse_args()

for filename in open(args.inputlist):
    if not filename.startswith("#"): 
        rootfile = TFile (filename.rstrip("\n"))
        print("Open "+filename)
        filebasename = os.path.basename(filename).rstrip(".root\n")
        h = rootfile.Get("h_average_wf")
	fileout = open(args.outdir+"/"+filebasename+".txt","w")
        fileout.write("##time [us]    voltage [V]\n")
	for bin in range(0,h.GetNbinsX()):
          print(str(h.GetBinLowEdge(bin+1))+"    "+str(h.GetBinContent(bin+1)))
	  fileout.write(str(h.GetBinLowEdge(bin+1))+"    "+str(h.GetBinContent(bin+1))+"\n")
        fileout.close()



