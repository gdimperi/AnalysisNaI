from ROOT import *
from array import array
import argparse
import os
import numpy as np

parser = argparse.ArgumentParser(description='')
#parser.add_argument('-i','--input', dest='input', type=str,
#                    default='', help='input file name')
parser.add_argument('-i','--inputdir', dest='inputdir', type=str,
                    default='', help='input directory path')
parser.add_argument('-o','--output', dest='output', type=str,
                    default='', help='output file name')
args = parser.parse_args()



directory = args.inputdir
#filename_in = args.input
filename_out = args.output


filenames = []
for filename in os.listdir(directory):
    if filename.startswith("C") and filename.endswith(".txt"):
        filenames.append(directory+"/"+filename)

filenames.sort()
print filenames


fileout  = TFile(filename_out,"recreate")

## Count n samples per waveform
ntot_samples = 0
ntot_wf = 0
with open(filenames[0]) as infp:
    for line in infp:
        if line.startswith("#"):
            ntot_wf += 1
        else:
            ntot_samples += 1

n_samples = ntot_samples/ntot_wf
print "ntot_samples: "+str(ntot_samples)
print "ntot_wf: "+str(ntot_wf)
print "n_samples per waveform: "+str(n_samples)

## initialize array
n = array( 'i', [ n_samples ] )
time = array("d", n_samples*[0.] )
wf = array("d", n_samples*[0.] )
sum_wf = array("d", n_samples*[0.] )
event_datetime = array( 'i',  6*[0] ) 
#print len(time)
#print len(wf)

## Create tree
t = TTree("wfTree", "tree with waveforms")
t.Branch( 'n', n, 'n/I' )
t.Branch( 'event_datetime', event_datetime, 'event_datetime[6]/I' )
t.Branch( 'time', time, 'time[n]/D' )
t.Branch( 'wf', wf, 'wf[n]/D' )
#t.Branch( 'sum_wf', sum_wf, 'sum_wf[n]/D' )

dict_datetime = {
        "Jan":1,
        "Feb":2,
        "Mar":3,
        "Apr":4,
        "May":5,
        "Jun":6,
        "Jul":7,
        "Aug":8,
        "Sep":9,
        "Oct":10,
        "Nov":11,
        "Dec":12
        }



## fill Tree
evcount = 0
samples = 0
for i in range(0,len(filenames)):
    for line in open(filenames[i]):
        if line.startswith("#"):
	    print(line)
	    #1,18-Sep-2019 16:40:37,0
            date_ = line.split(",")[1].split()[0]
            time_ = line.split(",")[1].split()[1]
            day_ = date_.split("-")[0] 
            month_ = dict_datetime[date_.split("-")[1] ]
            year_ = date_.split("-")[2]
            hour_ = time_.split(":")[0]
            min_ = time_.split(":")[1]
            sec_ = time_.split(":")[2]
            event_datetime[0] = int(day_)
            event_datetime[1] = int(month_)
            event_datetime[2] = int(year_)
            event_datetime[3] = int(hour_)
            event_datetime[4] = int(min_)
            event_datetime[5] = int(sec_)
            print(event_datetime)

        if not line.startswith("#") and not line.startswith("L") and not line.startswith("S") and not line.startswith("T"):
            time[samples-1] = np.double(line.split(",")[0])*1.e6
            wf[samples-1] = np.double(line.split(",")[1])
            #sum_wf[samples-1] += wf[samples-1]/(i+1) #wf average
            
	    #print(time[samples-1])
            samples +=1
        if samples == n_samples:
            t.Fill()
            evcount += 1
            samples = 0
            if evcount%100 == 0 :
                print("filling event "+str(evcount))


fileout.cd()
t.Write()
fileout.Close()
        

