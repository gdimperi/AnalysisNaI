from ROOT import *
from array import array

filein = TFile("singlePE_hist_R12669_FA0205.root")
fileout = open("plots_singlePE//fitresults.txt","w")
#filein = TFile("singlePE_hist_R12669_XXXX.root")
#fileout = open("plots_singlePE_PMTn_XXXX//fitresults.txt","w")
fileout.write("#HV    singlePE [pV s]   error [pV s]\n")
outdir = "plots_singlePE/"
#outdir = "plots_singlePE_PMTn_XXXX/"


x_ = []
xerr = []
y = []
yerr = []

for i in range(900,1350,50):
    hist = filein.Get("hist_"+str(i)+"V")

    if i<1000:
        x = RooRealVar("x","x",0,200)
    elif i<1150:
        x = RooRealVar("x","x",5,200)
    elif x<1250:
        x = RooRealVar("x","x",10,200)
    #    x = RooRealVar("x","x",5,200)
    else:
        x = RooRealVar("x","x",15,200)
    #    x = RooRealVar("x","x",10,200)

    dh = RooDataHist("dh","dh",RooArgList(x),hist) 


    meanPE =  RooRealVar("meanPE","Mean of Gaussian",0,0,100)
    sigmaPE = RooRealVar("sigmaPE","Width of Gaussian",3,0,50)
    gaussPE = RooGaussian("gaussPE","gauss(x,mean,sigma)",x,meanPE,sigmaPE)
    
    frac1 =  RooRealVar("frac1","fracion 1",0.5,0,100000)
    tau1 = RooRealVar("tau1","tau exp1",-0.5,-10,0)
    exp1 =  RooExponential("exponential 1","",x,tau1)
    frac2 =  RooRealVar("frac2","fracion 2",0.5,0,100000)
    tau2 = RooRealVar("tau2","tau exp2",-1,-10,0)
    exp2 =  RooExponential("exponential 2","",x,tau2)
    
    fracPE =  RooRealVar("fracPE","fracion gauss",0.5,0,100000)
    singlePE = RooAddPdf("singlePE","single PE",RooArgList(exp1,exp2,gaussPE),RooArgList(frac1,frac2,fracPE))
    #singlePE = RooAddPdf("singlePE","single PE",RooArgList(exp1,gaussPE),RooArgList(frac1,fracPE))
  
    # C o n s t r u c t   p l a i n   l i k e l i h o o d
    # ---------------------------------------------------

    # Construct unbinned likelihood
    nll = singlePE.createNLL(dh,RooFit.NumCPU(2))

    # Minimize likelihood w.r.t all parameters before making plots
    RooMinuit(nll).migrad() 

 
    # C o n s t r u c t   p r o f i l e   l i k e l i h o o d   i n   s i g m a P E 
    # -------------------------------------------------------------------------------
  
    # The profile likelihood estimator on nll for sigma_g2 will minimize nll
    # w.r.t all floating parameters except meanPE for each evaluation
    pll_meanPE = nll.createProfile(RooArgSet(meanPE))
  
    # Plot likelihood scan in meanPE
    frame2 = meanPE.frame(RooFit.Bins(10),RooFit.Range(0,100),RooFit.Title("LL and profileLL in meanPE"))
    nll.plotOn(frame2,RooFit.ShiftToZero()) 
    
    # Plot the profile likelihood in meanPE
    pll_meanPE.plotOn(frame2,RooFit.LineColor(kRed)) 
  
    # Adjust frame maximum for visual clarity
    frame2.SetMinimum(0) 
    frame2.SetMaximum(3) 


    # Make canvas and draw RooPlots
    c2 = TCanvas("profilell","",600, 600)
    c2.cd() 
    gPad.SetLeftMargin(0.15)
    frame2.GetYaxis().SetTitleOffset(1.4)
    frame2.Draw()
    c2.SaveAs(outdir+"/likelihood_"+str(i)+"V.pdf") 
     
    singlePE.fitTo(dh,RooFit.Extended(kTRUE))
    xframe = x.frame()
    dh.plotOn(xframe)
    singlePE.plotOn(xframe)
    fileout.write(str(i)+"     "+str(meanPE.getVal())+"   "+str(meanPE.getError())+"\n")
   
    # Draw the frame on the canvas
    c = TCanvas("c","",600,600)
    c.SetLeftMargin(0.15)
    xframe.Draw()
    xframe.GetXaxis().SetTitle("Waveform integral [pV s]")
    c.SaveAs(outdir+"/plot_singlePE_"+str(i)+"V.pdf")

    singlePE.Print("t")
    x_.append(i)
    xerr.append(0)
    y.append(meanPE.getVal()/50./1.6e-7)
    yerr.append(meanPE.getError()/50./1.6e-7)


vx = array("d",x_)
vy = array("d",y)
vxerr = array("d",xerr)
vyerr = array("d",yerr)


gr = TGraphErrors(len(vx),vx,vy,vxerr,vyerr)
gr.Draw("AP")
gr.GetXaxis().SetTitle("HV [V]")
gr.GetYaxis().SetTitle("Gain")
c.SetLogx(1)
c.SetLogy(1)

gain_f = TF1("gain_f","[0]*TMath::Power(x,[1])",900,1500)
gain_f.SetParameter(0,7.66020e-14)
gain_f.SetParameter(1,6.48140e+00)
for j in range(0,5):
  gr.Fit("gain_f","LMR")

gr.SetName("gain_gr")
c.SaveAs(outdir+"/gain.pdf")

gainfile = TFile("gain.root","recreate")
gainfile.cd()
gain_f.Write()
gr.Write()
gainfile.Close()
