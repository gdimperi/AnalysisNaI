from ROOT import *
from array import array
import argparse
import os


parser = argparse.ArgumentParser(description='')
#parser.add_argument('-i','--input', dest='input', type=str,
#                    default='', help='input file name')
parser.add_argument('-i','--inputlist', dest='inputlist', type=str,
                    default='', help='list of input files')
parser.add_argument('-o','--outputdir', dest='outputdir', type=str,
                    default='', help='output directory')
args = parser.parse_args()

gStyle.SetOptFit(1111)
c = TCanvas("c","",800,600)

for filename  in open(args.inputlist):
  print("Open "+filename)
  #if os.path.exists(filename):
  f = TFile.Open(filename.strip(),"read")
  h = f.Get("hist")
  #h.GetXaxis().SetRangeUser(-1000,5000)
  h.GetXaxis().SetRangeUser(-1000,9000)
  h.GetYaxis().SetRangeUser(0,400)
  c.cd()
  h.Draw()
  h.Fit("gaus","R","",5600,9000)
  #h.Fit("gaus","R","",2800,5000)
  #h.Fit("gaus","R","",1800,5000)
  c.SaveAs(args.outputdir+"/fit_"+os.path.basename(filename).rstrip(".root\n")+".pdf")
  c.Clear()

c.Close() 

