from ROOT import *
import numpy as np
from array import array
import argparse
import math
import os

parser = argparse.ArgumentParser(description='')
parser.add_argument('-i','--inputlist', dest='inputlist', type=str,
                    default='', help='input list file name')
parser.add_argument('-o','--outdir', dest='outdir', type=str,
                    default='', help='output dir name')
args = parser.parse_args()

gStyle.SetOptFit(1111)
gStyle.SetStatW(0.4)                
gStyle.SetStatH(0.2)

h_chi2 = TH1F("chi2","",100,0,100)
c_root = TCanvas("c_root","",800,600)

for filename in open(args.inputlist):
    if not filename.startswith("#"): 
        rootfile = TFile (filename.rstrip("\n"))
        print("Open "+filename)
        filebasename = os.path.basename(filename).rstrip(".root\n")
        h_average_wf = rootfile.Get("h_average_wf")
        h_baseline = rootfile.Get("h_baseline")
        baseline=h_baseline.GetMean()
        baseline_err=h_baseline.GetMeanError()
        print("baseline mean = "+str(baseline))
        print("baseline rms = "+str(baseline_err))
        
        for i in range(0,h_average_wf.GetNbinsX()):
          #h_average_wf.SetBinError(i+1,baseline_err+0.1*h_average_wf.GetBinContent(i+1))
          #h_average_wf.SetBinError(i+1,0.0001) ##arbitrary  error due to noisy PMT
          h_average_wf.SetBinError(i+1,0.00001) ##arbitrary  error due to noisy PMT
          #h_average_wf.SetBinError(i+1,0.0005) ##arbitrary  error due to noisy PMT
        
        c1 = TCanvas("c1","",800,600)
        c1.cd()
        h_average_wf.Draw("PE")
        integral = h_average_wf.Integral()
        c1.SaveAs(args.outdir+"/average_wf"+filebasename+".pdf")
        
        
        #########################################
        ## Fit method 1 : Roofit
        #########################################

        #x = RooRealVar("x","x",h_average_wf.GetBinCenter(h_average_wf.GetMaximumBin()+10),h_average_wf.GetBinCenter(h_average_wf.GetNbinsX())) ##start fit at 10 bins from max (binwidth=1 ns)
        ##x = RooRealVar("x","x",h_average_wf.GetBinCenter(h_average_wf.GetMaximumBin()+10),30) ##start fit at 10 bins from max (binwidth=1 ns)
        #dh = RooDataHist("dh","dh",RooArgList(x),h_average_wf) 
        #
        #
        #p0 = RooPolynomial("p0","p0",x) ;
        #a0 = RooRealVar("a0","a0",baseline,-0.001,0.001)
        #frac1 =  RooRealVar("frac1","fraction 1",0.002,0,1000)
        #tau1 = RooRealVar("tau1","tau exp1",-5,-100,0)
        ##invtau1 = RooFormulaVar("invtau1","","1/tau1",RooArgList(tau1))
        #exp1 =  RooExponential("exponential 1","",x,tau1)
        #frac2 =  RooRealVar("frac2","fraction 2",0.001,0,1000)
        #tau2 = RooRealVar("tau2","tau exp2",-0.5,-10,0)
        #exp2 =  RooExponential("exponential 2","",x,tau2)
       
        ##frac1.setConstant(kTRUE) 
        ##frac2.setConstant(kTRUE) 
        ##tau1.setConstant(kTRUE)
        ##tau2.setConstant(kTRUE)

        #decaytime = RooAddPdf("decaytime","decay time",RooArgList(p0,exp1,exp2),RooArgList(a0,frac1,frac2))
        ##decaytime =  RooAddition("decaytime","decay time",RooArgList(p0,exp1,exp2),RooArgList(a0,frac1,frac2))
        #fit_component_const = RooArgSet(p0)
        #fit_component_exp1 = RooArgSet(exp1)
        #fit_component_exp2 = RooArgSet(exp2)
        #
        #ll = RooLinkedList()
        ##fitr = decaytime.fitTo(dh,RooFit.Save())
        ##fitr.Print()
        #decaytime.chi2FitTo(dh,ll)
        ##decaytime.chi2FitTo(dh,RooFit.Minimizer("Minuit2","Migrad"),RooFit.SumW2Error(kTRUE))
        ##decaytime.fitTo(dh,RooFit.Minimizer("Minuit2","Migrad"),RooFit.SumW2Error(kTRUE))
	#xframe = x.frame()
        #xframe.GetXaxis().SetRange(0,20)
        ##xframe.GetYaxis().SetRange(1e-6,1)
        ##xframe.SetMinimum(1e-6)
        ##xframe.SetMaximum(0.005)
        #dh.plotOn(xframe)
        #decaytime.plotOn(xframe)
        #decaytime.plotOn(xframe,RooFit.Components(fit_component_const),RooFit.LineStyle(2),RooFit.LineColor(2))
        #decaytime.plotOn(xframe,RooFit.Components(fit_component_exp1),RooFit.LineStyle(2),RooFit.LineColor(3))
        #decaytime.plotOn(xframe,RooFit.Components(fit_component_exp2),RooFit.LineStyle(2),RooFit.LineColor(kCyan))
        #
        ##p0.plotOn(xframe,RooFit.LineStyle(kDashed),RooFit.LineColor(kRed))
        ##exp1.plotOn(xframe,RooFit.LineStyle(kDashed),RooFit.LineColor(kGreen))
        ##exp2.plotOn(xframe,RooFit.LineStyle(kDashed),RooFit.LineColor(kCyan))
        #
        ## Draw the frame on the canvas
        #c = TCanvas("c","",800,600)
        #c.SetLeftMargin(0.15)
        #xframe.GetYaxis().SetTitleOffset(1.5)
        #xframe.Draw()
        #xframe.GetXaxis().SetTitle("Decay time [us]")
        #c.SetLogy()
        #c.SaveAs(args.outdir+"/plot_decaytime_"+filebasename+".pdf")
        #c.Close()

        #decaytime.Print("t")
	##chi2 = xframe.chiSquare("decaytime", "dh", 5)
        #
        ##chi2 = RooChi2Var("chi2", "chi2", decaytime, dh)
	##print("reduced chi2 = "+str(chi2.getVal()/dh.numEntries()))
	##h_chi2.Fill(chi2.getVal()/dh.numEntries())
        #         
	#
	#txtfileout = open(args.outdir+"/fitresult_"+filebasename+".txt","w") 
        #txtfileout.write("a0 =     "+str(a0.getVal())+" +/-  "+str(a0.getError())+"\n")
        #txtfileout.write("frac1 =    "+str(frac1.getVal())+" +/-  "+str(frac1.getError())+"\n")
        #txtfileout.write("tau1 =    "+str(tau1.getVal())+" +/-  "+str(tau1.getError())+"\n")
        #txtfileout.write("frac2 =    "+str(frac2.getVal())+" +/-"+str(frac2.getError())+"\n")
        #txtfileout.write("tau2 =    "+str(tau2.getVal())+" +/-  "+str(tau2.getError())+"\n")
        ##txtfileout.write("fit chi2 =    "+str(chi2.getVal()/dh.numEntries())+"\n")
        #txtfileout.close()

        ########################################
        # Fit method 1 : Root
        ########################################
        f_decaytime = TF1("f_decaytime","[0]+[1]*TMath::Exp(-x*[2])+[3]*TMath::Exp(-x*[4])",h_average_wf.GetBinCenter(h_average_wf.GetMaximumBin()+10),h_average_wf.GetBinCenter(h_average_wf.GetNbinsX())) ##start fit at 10 bins from max (binwidth=1 ns)
        f_a0 = TF1("f_a0","[0]",h_average_wf.GetBinCenter(h_average_wf.GetMaximumBin()+10),h_average_wf.GetBinCenter(h_average_wf.GetNbinsX()))
        f_exp1 = TF1("f_exp1","[0]*TMath::Exp(-[1]*x)",h_average_wf.GetBinCenter(h_average_wf.GetMaximumBin()+10),h_average_wf.GetBinCenter(h_average_wf.GetNbinsX()))
        f_exp2 = TF1("f_exp2","[0]*TMath::Exp(-[1]*x)",h_average_wf.GetBinCenter(h_average_wf.GetMaximumBin()+10),h_average_wf.GetBinCenter(h_average_wf.GetNbinsX()))
        f_decaytime.SetParameter(0,baseline)
        f_decaytime.SetParameter(1,0.004)
        f_decaytime.SetParameter(2,5)
        f_decaytime.SetParameter(3,0.001)
        f_decaytime.SetParameter(4,0.01)
        f_decaytime.SetParLimits(0,-0.01,0.01)
        f_decaytime.SetParLimits(1,0.00001,1000)
        f_decaytime.SetParLimits(2,0.,1000)
        f_decaytime.SetParLimits(3,0.,1000)
        f_decaytime.SetParLimits(4,0.,1000)
        f_decaytime.SetParName(0,"a0")       
        f_decaytime.SetParName(1,"frac1")
        f_decaytime.SetParName(2,"1/tau1")
        f_decaytime.SetParName(3,"frac2")
        f_decaytime.SetParName(4,"1/tau2")
	#
        #f_decaytime.FixParameter(2,3.51296192077) #12K
        #f_decaytime.FixParameter(2,3.9386269659) #25K
        #f_decaytime.FixParameter(2,3.47932379367) #50K
        #f_decaytime.FixParameter(2,3.57043924727) #75K
        #f_decaytime.FixParameter(2,3.24170793252) #100K
        #f_decaytime.FixParameter(2,2.89775498683) #125K
        #f_decaytime.FixParameter(2,2.76422065903) #150K
        #f_decaytime.FixParameter(2,3.08426025218) #173K
        f_decaytime.FixParameter(2,3.4555150557) #197K
        #f_decaytime.FixParameter(2,3.63966405396) #222K

        #f_decaytime.FixParameter(3,0)
        #f_decaytime.FixParameter(4,1)



        result = h_average_wf.Fit("f_decaytime","RS")
        result.Print()
	txtfileout = open(args.outdir+"/fitresult_"+filebasename+".txt","w") 
        txtfileout.write("a0 =     "+str(f_decaytime.GetParameter(0))+" +/-  "+str(f_decaytime.GetParError(0))+"\n")
        txtfileout.write("frac1 =    "+str(f_decaytime.GetParameter(1))+" +/-  "+str(f_decaytime.GetParError(1))+"\n")
        txtfileout.write("tau1 =    "+str(f_decaytime.GetParameter(2))+" +/-  "+str(f_decaytime.GetParError(2))+"\n")
        txtfileout.write("frac2 =    "+str(f_decaytime.GetParameter(3))+" +/-"+str(f_decaytime.GetParError(3))+"\n")
        txtfileout.write("tau2 =    "+str(f_decaytime.GetParameter(4))+" +/-  "+str(f_decaytime.GetParError(4))+"\n")
        txtfileout.write("fit chi2 =    "+str(result.Chi2())+"\n")
        txtfileout.close()


        f_a0.SetParameter(0,f_decaytime.GetParameter(0))
        f_exp1.SetParameter(0,f_decaytime.GetParameter(1))
        f_exp1.SetParameter(1,f_decaytime.GetParameter(2))
        f_exp2.SetParameter(0,f_decaytime.GetParameter(3))
        f_exp2.SetParameter(1,f_decaytime.GetParameter(4))

        c_root.cd()
        h_average_wf.GetXaxis().SetTitle("Time [#mus]")
        h_average_wf.GetYaxis().SetTitle("Voltage [V]")
        h_average_wf.GetYaxis().SetTitleOffset(1.4)
        h_average_wf.Draw("PE")
        #h_average_wf.GetXaxis().SetRangeUser(0,20)
        h_average_wf.GetXaxis().SetRangeUser(0,h_average_wf.GetBinLowEdge(h_average_wf.GetNbinsX()))
        f_decaytime.Draw("same")
        f_a0.SetLineColor(4)       
        f_a0.SetLineStyle(2)       
        f_exp1.SetLineColor(3)       
        f_exp1.SetLineStyle(2)       
        f_exp2.SetLineColor(kCyan)       
        f_exp2.SetLineStyle(2)       
        f_a0.Draw("same") 
        f_exp1.Draw("same") 
        f_exp2.Draw("same") 
        leg = TLegend(0.6,0.3,0.85,0.5)
        #leg.SetLineColor(0)
        leg.AddEntry(h_average_wf,"data","pe")
        leg.AddEntry(f_decaytime,"tot. fit","l")
        leg.AddEntry(f_a0,"const.","l")
        leg.AddEntry(f_exp1,"exp.1","l")
        leg.AddEntry(f_exp2,"exp.2","l")
        leg.Draw() 

        c_root.SetLogy(0)
        c_root.SaveAs(args.outdir+"/plot_decaytime_root_"+filebasename+".pdf")  
        c_root.SetLogy(1)
        c_root.SaveAs(args.outdir+"/plot_decaytime_root_"+filebasename+"_log.pdf")  
        c_root.Clear()

c_root.Close()

#c_chi2 = TCanvas("c_chi2","",800,600)
#c_chi2.cd()
#h_chi2.Draw()
#c_chi2.SaveAs(args.outdir+"/chi2_longwf.pdf")
#c_chi2.Close() 
