from ROOT import *
import numpy as np
from array import array
import argparse
import math



def findmin(array):
    array_sort = np.sort(array)
    print array_sort
    minimum = array_sort[0]
    idx_min, = np.where(array == minimum)
    return minimum, idx_min[0]
    

def sigma_baseline(time,wf):
    wftmp = np.array(wf)
    mean =0
    var = 0
    for i in range(0,1000):
        mean += wftmp[i]/1000
    for i in range(0,1000):
        var += (wftmp[i]-mean)*(wftmp[i]-mean)/1000.
    sigma = math.sqrt(var)
    return mean,sigma


def find_starttime(time,wf):
    wftmp = np.array(wf)
    wfmin = wftmp.min()
    #index_min = wftmp.argmin()
    index_min, = np.where(wftmp==wfmin)
    ## define threshold as 1/100 of the minimum
    #threshold = wfmin/100.
    threshold = -1*0.0005
    baseline,sigma = sigma_baseline(time,wf)
    #threshold = baseline-10*sigma 
    array_index_, = np.where(wftmp <= threshold)
    starttime_idx = array_index_[0]
    return starttime_idx

def calculate_area(time,wf,idx1,idx2):
    timeinterval = time[idx2]-time[idx1]
    timebin = time[1]-time[0]
    area = 0
    baseline = sigma_baseline(time,wf)[0]
    for i in range(idx1,idx2):
        area += -1.e3*timebin * (wf[i]-baseline)   ##nVs
    print("area: "+str(area))
    return area ##nVs





parser = argparse.ArgumentParser(description='')
parser.add_argument('-i','--input', dest='input', type=str,
                    default='', help='input file name')
parser.add_argument('-o','--output', dest='output', type=str,
                    default='', help='output file name')
args = parser.parse_args()



filename_in = args.input
filename_out = args.output
filein = TFile(filename_in,"read")
fileout = TFile(filename_out,"recreate")

t = filein.Get("wfTree")
nentries = t.GetEntries()

## test
t.GetEntry(1)
time_tmp = t.time
wf_tmp = t.wf
width_t = (time_tmp[1]-time_tmp[0])

wftmp = np.array(t.wf)
#wfmin = wftmp.min()
#index_min = wftmp.argmin()
wfmin = findmin(wftmp)[0]
#index_min, = np.where(wftmp==wfmin)
wfminidx = np.array(findmin(wftmp)[1])
print wfmin
baseline,sigma = sigma_baseline(time_tmp,wf_tmp)
width_V = abs(wfmin-baseline)/1000.
#print wfmin-20*width_V
#h2_persistency = TH2F("h2_persistency","", len(t.time),t.time[0],t.time[len(t.time)-1],200,wfmin-20*width_V,baseline+20*width_V)
h2_persistency = TH2F("h2_persistency","", 10000,-0.5,5,5500,10*wfmin,baseline+20*width_V)
max_area = 10
h_area = TH1F("h_area","",200,0,1.2*max_area)
h_area.GetXaxis().SetTitle("#Area [nVs]")

areas = []
wftmp =  np.array(t.wf)
average_wf = np.zeros(len(wftmp))

h_baseline = TH1F("h_baseline","",200,-0.001,0.001)

totarea=0
iwf = 0
#for i in range(0,t.GetEntries()):
for i in range(0,200): #FIXME
    if i%10==0:
        print("Analizing event "+str(i))
    #for i in range(0,100):
    #print datetimetmp[5]
    t.GetEntry(i)

    time = t.time
    wf = t.wf
    wftmp = np.array(wf)
    wfmin = wftmp.min()
    baseline,sigma = sigma_baseline(time,wf)
    
    threshold = baseline -10*sigma
    threshold_max = baseline + 10*sigma
    #print threshold_max
    array_index, = np.where(wftmp <= threshold)
    index_min, = np.where(wftmp==wfmin)
    #print("index min:"+str(index_min))

    starttime = find_starttime(time,wf)
    
    area = -999
    timebin = time[1]-time[0]
    area = calculate_area(time,wf,index_min[0]-125,len(time)-1)    
    #print("threshold: "+str(threshold))
    #print("time minimum = "+str(time[index_min[0]]))
    #print("area "+str(area))

    areas.append(area)
    h_area.Fill(area)

    threshold = -1*0.003
    
    array_index_, = np.where(wftmp <= threshold)

    print("samples under threshold "+str(len(array_index_)))
    
    totarea += area
    min_over_area = -1*wfmin/area
    print("min/area "+str(min_over_area))
    if area>0.3 and len(array_index_)>100 and min_over_area<0.03:
         for j in range(0,len(time)):
            #print("time: "+str(time[j])+"   ampl: "+str(wf[j]))
            h2_persistency.Fill(float(time[j]),float(wf[j]))
            average_wf[j]+=wf[j] #wf sum
            h_baseline.Fill(baseline)
	 iwf +=1

print("average over "+str(iwf)+" waveforms with area >0.1 nVs")
#average_wf = -1*average_wf/totarea ## change sign and average
#FIXME
average_wf = average_wf/totarea ## change sign and average
## cross check (should be =1 )
area_ave = calculate_area(time,average_wf,index_min[0]-125,len(time)-1)    

t.GetEntry(0)
time = t.time
timebin = time[1]-time[0]
nsamples = len(time)
print("wf time range: "+str(time[0])+" - "+str(time[0]+timebin*nsamples)+"    timebin = "+str(timebin)+" us")
h_average_wf =  TH1F("h_average_wf","",len(time),time[0],time[0]+timebin*nsamples)
#h_wf =  TH1F("h_wf","",len(time),time[0],time[0]+timebin*nsamples)
for i in range(0,len(time)):
    #if average_wf[i]>0:
    #   print("set bin content "+str(i)+" : "+str(average_wf[i]))
    h_average_wf.SetBinContent(i,average_wf[i])
    h_average_wf.SetBinError(i,0.1*average_wf[i])
    #h_wf.SetBinContent(i,wf[i]) #FIXME debug
    
h_average_wf.Rebin(10) #10 Gs/sec--> 1 Gs/sec

h_baseline.Write()
h_average_wf.Write()
h2_persistency.Write()
h_area.Write()
fileout.Close()

