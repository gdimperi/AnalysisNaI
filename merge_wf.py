import argparse
import os
import fileinput

parser = argparse.ArgumentParser(description='')
parser.add_argument('--inputdir', dest='inputdir', type=str,
                    default='', help='input directory path')
parser.add_argument('-o','--output', dest='output', type=str,
                    default='', help='output file name')
args = parser.parse_args()


directory = args.inputdir
fileoutname = args.output

filenames = []
for filename in os.listdir(directory):
    if filename.startswith("C") and filename.endswith(".txt"):
        filenames.append(directory+"/"+filename)

filenames.sort()
print filenames

fileout = open(fileoutname,"w")
for line in fileinput.input(filenames):
    if not line.startswith("L") and not line.startswith("S") and not line.startswith("T"):
        fileout.write(line)

fileout.close()
