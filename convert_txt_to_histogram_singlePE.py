from ROOT import *
import argparse


parser = argparse.ArgumentParser(description='')
parser.add_argument('-i','--input', dest='input', type=str,
                    default='', help='input list file name')
parser.add_argument('-o','--output', dest='output', type=str,
                    default='', help='output file name')
parser.add_argument('--roma', dest='roma', action="store_true",
                    default='', help='output file name')
#parser.add_argument('--histname', dest='histname', type=str,
#                    default='', help='histogram name')
args = parser.parse_args()


def invertsign(num):
    num *= -1
    return num


def main():

    time = 1

    inputlist = open(args.input,"r")
    out_file = TFile(args.output,'recreate')
    out_file.cd()
    for filename_in in inputlist:
      filename_in = filename_in.rstrip()
      xbins_temp = []
      xbins = []
      y = []
      nlines = 0
      in_file = open(filename_in,'r')
      for line in in_file.readlines():
        if not line.startswith('#') and not line.startswith('LECROY') and not line.startswith("Segment") and not line.startswith("Time") and nlines <2:
          if not args.roma:
            xbins_temp.append(invertsign(float(line.split(' ')[0]))*1e12 )
          else:
            xbins_temp.append(invertsign(float(line.split(',')[0]))*1e12 )
          if nlines == 1:
            binwidth = float(xbins_temp[1])-float(xbins_temp[0])
          nlines += 1
      
      nlines=0  
      in_file2 = open(filename_in,'r')
      for line in reversed(in_file2.readlines()):
        if not line.startswith('#') and not line.startswith('LECROY') and not line.startswith("Segment") and not line.startswith("Time"):
            if not args.roma:
                xbins.append(invertsign(float(line.split(' ')[0]))*1e12 )
                y.append(float(line.split(' ')[1])/time)
            else:
                xbins.append(invertsign(float(line.split(',')[0]))*1e12 )
                y.append(float(line.split(',')[1])/time)

            nlines += 1

      namefile_list =  filename_in.split("/")
      namefile = namefile_list[len(namefile_list)-1]
      if not args.roma:
        hist = TH1F("hist_"+namefile.split("_")[1],"",nlines,xbins[0],xbins[nlines-1]+binwidth)
      else:
        hist = TH1F("hist_"+namefile.split("_")[1].split("V")[0]+"V","",nlines,xbins[0],xbins[nlines-1]+binwidth)
        
      nEvt = 0
      for i in range(0,nlines):
          hist.SetBinContent(i+1,y[i])
          nEvt+=(y[i]*time)
      hist.GetXaxis().SetTitle('pV s')
      hist.GetYaxis().SetTitle('Counts')
      hist.Write()
      print "N events: %d" % nEvt
      
    out_file.Close()

if __name__ == "__main__":
    main()
